//
//  NgosLogin.swift
//  HungerReliefNGOS
//
//  Created by Kommula,Priyanka on 4/5/16.
//  Copyright © 2016 Valleshetti. All rights reserved.
//



import Foundation
class NgosLogin:NSObject{
    var emailId:String
    var password:String
    var entityId: String?
    
    init(emailId:String, password:String) {
        self.emailId = emailId
        self.password = password
    }
    override func hostToKinveyPropertyMapping() -> [NSObject : AnyObject]! {
        return [
            "entityId" : KCSEntityKeyId, //the required _id field
            "emailId" : "emailId",
            "password" : "password",
            
            
        ]
    }
    
    
}

