//
//  Ngos.swift
//  HungerReliefNGOS
//
//  Created by Kommula,Priyanka on 4/5/16.
//  Copyright © 2016 Valleshetti. All rights reserved.
//

import Foundation



class Ngos:NSObject{
    var ngosName:String
        var emailId:String
        var password:String
        var address:String
        var city:String
        var state:String
        var country:String
        var phoneNumber:String
        var entityId: String?
        
        init(ngosName:String,emailId:String,password:String,address:String,city:String,state:String,country:String,phoneNumber:String){
            
            self.ngosName = ngosName
            self.emailId = emailId
            self.password = password
            self.address = address
            self.city = city
            self.state = state
            self.country = country
            self.phoneNumber = phoneNumber
        }
        override func hostToKinveyPropertyMapping() -> [NSObject : AnyObject]! {
            return [
                "entityId" : KCSEntityKeyId, //the required _id field
                "ngosName" : "ngosName",
                "emailId" : "emailId",
                "password" : "password",
                "address":"address",
                "city":"city",
                "state":"state",
                "country":"country",
                "phoneNumber":"phoneNumber"
                
                
            ]
        }
        
}
