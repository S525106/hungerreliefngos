//
//  RegisterViewController.swift
//  Hunger Relief NGO'S
//
//  Created by Valleshetti,Anwesh on 3/15/16.
//  Copyright © 2016 Valleshetti,Anwesh. All rights reserved.
//


import UIKit

class SignUpViewController: UIViewController ,Operation{
    
  
    @IBOutlet weak var ngosNameTF: UITextField!
    @IBOutlet weak var emailIdTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var addressTF: UITextField!
    @IBOutlet weak var cityTF: UITextField!
    @IBOutlet weak var stateTF: UITextField!
    @IBOutlet weak var phoneNumberTF: UITextField!
    @IBOutlet weak var countryTF: UITextField!
    @IBOutlet weak var confirmPasswordTF: UITextField!
    
    var kinveyClientOperation:KinveyOperation!
    var  ngos:Ngos!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationItem.title = "Registration"
        self.navigationController?.navigationItem.hidesBackButton = true
        
        kinveyClientOperation = KinveyOperation(operation:self)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func isValidEmail(testStr:String) -> Bool {
        
        //  println("validate emilId: \(testStr)")
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        let result = emailTest.evaluateWithObject(testStr)
        
        return result
        
    }
    @IBAction func Register(sender: AnyObject) {
        
        
        if (ngosNameTF.text!.isEmpty || emailIdTF.text!.isEmpty || passwordTF.text!.isEmpty){
            self.displayAlertControllerWithTitle("Registration Failed", message: "These fields are Required")
        }
        else if  passwordTF.text != confirmPasswordTF.text! {
            self.displayAlertControllerWithTitle("Mismatch", message: "password doesnot match")
        }
        else if (!isValidEmail(emailIdTF.text!))
        {
            self.displayAlertControllerWithTitle("Invalid Email", message: "insert proper email address")
        }
            
        else{
            ngos = Ngos(ngosName: ngosNameTF.text!, emailId: emailIdTF.text!, password: passwordTF.text!, address: addressTF.text!, city: cityTF.text!, state: stateTF.text!, country: countryTF.text!, phoneNumber: phoneNumberTF.text!)
            kinveyClientOperation.Register(ngos)
            
        }
    }
    func SignupSuccess() {
        self.displayAlertControllerWithSuccess("Registration successful", message: "Welcome!")
    }
    func onError(message: String) {
        self.displayAlertControllerWithTitle("Registration failed", message: message)
    }
    func noActiveUser() {
        print("noActiveUser")
        
    }
    
    func displayAlertControllerWithSuccess(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler:{ action in self.performSegueWithIdentifier("registerSuccess", sender: self)}))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
        
    }
    func displayAlertControllerWithTitle(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler:{(action:UIAlertAction)->Void in  }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
        
    }
    
    
    
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
}
