//
//  ReviewViewController.swift
//  Hunger Relief NGO'S
//
//  Created by Valleshetti,Anwesh on 3/15/16.
//  Copyright © 2016 Valleshetti,Anwesh. All rights reserved.
//


import UIKit






class ReviewViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,Operation {
    
    var kinveyClientOperation:KinveyOperation!
    
    var array:[Item] = []
    var myitems:[Item] = []
    var userId:String!
     let defaults = NSUserDefaults.standardUserDefaults()
    
    @IBOutlet weak var tableView: UITableView!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "REVIEW ITEMS"
        
        //self.navigationItem.backBarButtonItem=nil
        kinveyClientOperation = KinveyOperation(operation:self)
        // kinveyClientOperation.FetchItem(userId)
                
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    override func viewWillAppear(animated: Bool) {
       
        kinveyClientOperation.FetchItem(userId)
        self.tableView.reloadData()
    }
//    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
//        if editingStyle == UITableViewCellEditingStyle.Delete {
//            array.removeAtIndex(indexPath.row)
//            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
//        }
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return array.count
    }
    //    override func viewWillAppear(animated: Bool) {
    //        kinveyClientOperation.FetchItem()
    //    }
    
    func fetchitem(item: Item) {
        array.append(item)
       
        
        
        self.tableView.reloadData()
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reviewcell", forIndexPath: indexPath)
        //kinveyClientOperation.FetchItem()
        
        
        
        
        let nameLbl : UILabel = cell.viewWithTag(11) as! UILabel
        
        let typeLbl : UILabel = cell.viewWithTag(12) as! UILabel // function viewWithTag(Int) to update the labels in the table.
        
        let quantityLbl:UILabel = cell.viewWithTag(78) as! UILabel
        
        nameLbl.text = array[indexPath.row].itemName
        
        typeLbl.text = array[indexPath.row].type
        
        quantityLbl.text = array[indexPath.row].quantity
        
        
       
        let stop : UIButton = UIButton(type: UIButtonType.RoundedRect) as UIButton
        
                stop.frame = CGRectMake(500, 0, 100, 50)
        
                //stop.center = CGPoint(x: view.bounds.width / 1.0, y: view.bounds.height / 1.0)
        
                stop.backgroundColor = UIColor.redColor()
 
        
                stop.tag = indexPath.row
        
                stop.addTarget(self, action: "Add:", forControlEvents:
        
                    UIControlEvents.TouchUpInside)
        
                stop.setTitle("Add to Cart", forState: UIControlState.Normal)
        
        print(stop)
                cell.addSubview(stop)
        
        return cell
    }
    
    func Add(sender:UIButton) {
        
     
        let indexPath  = NSIndexPath(forRow: sender.tag, inSection: 0)
        
        
       myitems.append(array[indexPath.row])
        
       
        
        print("**********\(myitems.count)")
        
        array[indexPath.row].flag = true
         var item = array[indexPath.row]
        
        kinveyClientOperation.SaveItem(item)
//         let query = KCSQuery(onField: "flag", withExactMatchForValue:array[indexPath.row] )
//        
//        kinveyClientOperation.store3.saveObject(
//                    query,
//                    withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
//                        if errorOrNil != nil {
//                            //save failed
//                            print("Save failed, with error: %@", errorOrNil.localizedFailureReason)
//                        } else {
//                            //save was successful
//                            print("Successfully saved event (id='%@').", (objectsOrNil[0] as! NSObject).kinveyObjectId())
//                        }
//                    },
//                    withProgressBlock: nil
//                )
//                
//            }
//       
//    
        
        
//       let query = KCSQuery(onField: "flag", withExactMatchForValue: )
        
    
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */


}
