//
//  Item.swift
//  HungerReliefNGOS
//
//  Created by Kommula,Priyanka on 4/10/16.
//  Copyright © 2016 Valleshetti. All rights reserved.
//

import Foundation


class Item:NSObject{
    var itemName:String
    var type:String
    var quantity:String
    var emailId:String
    var entityId: String?
    var flag:Bool = false
    
    
    override init(){
        self.itemName = ""
        self.type = ""
        self.quantity = ""
        self.emailId = ""
        
    }
    
    init(itemName:String,type:String,quantity:String,emailId:String){
        
        self.itemName = itemName
        self.type = type
        self.quantity = quantity
        self.emailId = emailId
        
    }
    override func hostToKinveyPropertyMapping() -> [NSObject : AnyObject]! {
        return [
            "entityId" : KCSEntityKeyId, //the required _id field
            "itemName" : "itemName",
            "type" : "type",
            "quantity" : "quantity",
            "emailId" :"emailId",
            "flag":"flag"
            
        ]
    }
    
}
