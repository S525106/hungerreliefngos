//
//  ItemsViewController.swift
//  Hunger Relief NGO'S
//
//  Created by Valleshetti,Anwesh on 3/15/16.
//  Copyright © 2016 Valleshetti,Anwesh. All rights reserved.
//


import UIKit

class ItemsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource ,Operation{
    
    var kinveyClientOperation:KinveyOperation!
    var restaurant:Restaurant!
    var listarray:[Item] = []
    var restaurantName:String!
    
    @IBOutlet weak var tableView: UITableView!
    var restaurantArray:[Restaurant] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        kinveyClientOperation = KinveyOperation(operation:self)
        kinveyClientOperation.FetchRestaurant()
        
        

        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    func fetch(restaurant:Restaurant) {
        restaurantArray.append(restaurant)
        
        
        self.tableView.reloadData()
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return restaurantArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("tablelist", forIndexPath: indexPath)
        //kinveyClientOperation.FetchItem()
        
        
        
        
        let nameLbl : UILabel = cell.viewWithTag(11) as! UILabel
        
        
        nameLbl.text = restaurantArray[indexPath.row].restaurantName
        
        
        return cell
    }
    
      override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "review"
        {
            if let destinationVC = segue.destinationViewController as? ReviewViewController{
                if let displayIndex = tableView.indexPathForSelectedRow?.row {
                   
                    destinationVC.userId = restaurantArray[displayIndex].emailId
                    
                    restaurantName = restaurantArray[displayIndex].restaurantName
                    
                    
                }
                self.listarray = destinationVC.myitems
            }
        }
    }

   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
